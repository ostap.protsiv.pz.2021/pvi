<?php
    header('Access-Control-Allow-Origin: *');
    $data = json_decode(file_get_contents("php://input"));
    $errors = array();
    if (empty($data->firstName)) {
        $errors["firstName"] = "First name cannot be empty";
    }
    if (empty($data->lastName)) {
        $errors["lastName"] = "Last name cannot be empty";
    }
    if(empty($data->group)){
        $errors["group"]= "Group cannot be empty";
    }elseif (strlen($data->group) > 5 ) {
        $errors["group"]= "Name of the group cannot be longer than 5";
    }
    if (empty($data->gender)) {
        $errors["gender"] = "Gender cannot be empty";
    }
    if (empty($data->birthday)) {
        $errors["birthday"] = "Birthday cannot be empty";
    }else {
        $birthday = new DateTime($data->birthday);
        $currentDate = new DateTime();
        $diff = $currentDate->diff($birthday);
        if ($diff->y <= 16) {
            $errors["birthday"] = "Student cannot be younger than 16";
        }
    }

    header('Content-Type: application/json');
    if (count($errors) > 0) {
        // echo json_encode(array('success' => false, 'errors' => $errors));
        echo json_encode(array('errors' => $errors));
    } else {
        // echo json_encode(array('success' => true, 'data' => $data));
        echo json_encode(array('data' => $data));
    }
?>