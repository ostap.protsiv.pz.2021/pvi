
$(document).ready(function(){
  let lastUserId = 0;

  function getFieldsFromModal() {
    return {
      group : $("#groupName").val(),
      firstName : $("#student-first-name").val(),
      lastName : $("#student-last-name").val(),
      gender : $("#gender-select").val(),
      birthday : $("#birthday-input").val()
    }
  }

  function PutFieldsIntoModal(dataId) {
    const [groupTd,nameTd,genderTd,birthdayTd] = $(`tbody tr[data-id=${dataId}]`).children('td');
    $("#groupName").val(groupTd.textContent);
    const [firstName, lastName] = nameTd.textContent.split(" ");
    $("#student-first-name").val(firstName);
    $("#student-last-name").val(lastName);
    $("#gender-select").val(genderTd.textContent);
    $("#birthday-input").val(birthdayTd.textContent);
  }

  function InputIntoTable({group,firstName,lastName,gender,birthday}) {
    const htmlToInput= 
        `                  
        <tr data-id="${++lastUserId}">
        <th scope="row" class="text-center">
          <input
            type="checkbox"
            name="tableCheck"
            class="table-check"
          />
        </th>
        <td>${group}</td>
        <td>${firstName +" " +lastName}</td>
        <td>${gender}</td>
        <td>${birthday}</td>
        <td><div class="status rounded-circle d-inline-block"></div></td>
        <td>
        <button class="btn ms-auto d-inline-block border border-2 edit call-form" data-bs-toggle="modal" data-bs-target="#addEditStudent" data-id="${lastUserId}">
          <i class="bi bi-pencil icons"></i>
        </button>
        <button class="btn ms-auto d-inline-block border border-2 remove call-form" data-id="${lastUserId}">
          <i class="bi bi-x-lg icons"></i>
        </button>
        </td>
      </tr>`
      $('#studentTable tbody').append(htmlToInput);
  }

  function UpdateStudentFields({group,firstName,lastName,gender,birthday,dataId}) {
      const [groupTd,nameTd,genderTd,birthdayTd,optionsTd] = $(`tbody tr[data-id=${dataId}]`).children('td');
      groupTd.textContent = group,
      nameTd.textContent = firstName + " " + lastName;
      genderTd.textContent = gender;
      birthdayTd.textContent = birthday;
      $(optionsTd).children('div')[0].style.backgroundColor = 'greenyellow'
    }

    function SetErrotMessage(errorKey,errMessage) {
      console.log(errorKey,errMessage);
      if(errorKey == 'birthday'){
        $("#student-birthday-error").text(errMessage).prop('hidden', false);
      }else if(errorKey == 'group'){
        $("#group-error").text(errMessage).prop('hidden', false); 
      }else if(errorKey == 'birthday'){
        $("#student-birthday-error").text(errMessage).prop('hidden', false); 
      }else if(errorKey == 'firstName'){
        $("#student-first-name-error").text(errMessage).prop('hidden', false); 
      }else if(errorKey == 'lastName'){
        $("#student-last-name-error").text(errMessage).prop('hidden', false); 
      }else if(errorKey == 'gender'){
        $("#student-gender-error").text(errMessage).prop('hidden', false); 
      }

    }
    function ClearError(elem) {
      const errorDiv = elem.siblings('.error').first();
      errorDiv.text("").prop('hidden', true);
    }

    $(document).on('click','#studentTable .remove',function(){
        $(this).parent().parent().remove()
    })

    $(".table-div").on('click','.call-form',function(){
        const dataId = $(this).attr('data-id')
        $("#gender-select").val("");
        if(dataId ===""){
          $('.modal-title').text('Add a student');
        }else{
          $('.modal-title').text('Edit a student');
          PutFieldsIntoModal(dataId)
          $('#addEditForm').attr('data-id',dataId);
        }
    })

    $("#addEditForm").submit(async function(event){
      event.preventDefault();
      const dataId = $(this).attr('data-id')
      if(dataId ===""){
        const dataFromInput = getFieldsFromModal();
          const response = await fetch("http://localhost:5500/validate-form.php",{
            method:"POST",
            body:JSON.stringify(dataFromInput)
          })
          const responseText = await response.text();
          const responseObj = JSON.parse(responseText).errors;
          if(responseObj){
            for (const key in responseObj) {
              SetErrotMessage(key,responseObj[key]);
            }
          }else{
            $("#addEditStudent").modal("hide");
          }
          
        InputIntoTable({...dataFromInput,dataId:lastUserId});
      }else{
        const dataFromInput = getFieldsFromModal();
        const response = await fetch("http://localhost:5500/validate-form.php",{
          method:"POST",
          body:JSON.stringify(dataFromInput)
        })
        const responseText = await response.text();
        const responseObj = JSON.parse(responseText).errors;
        if(responseObj){
          for (const key in responseObj) {
            SetErrotMessage(key,responseObj[key]);
          }
        }else{
          $("#addEditStudent").modal("hide");
        }
        UpdateStudentFields({...dataFromInput, dataId})
      }
  })
  
  $("#addEditStudent input, #addEditStudent select").change(function (params) {
    ClearError($(this));
  })

  $("#addEditStudent").on("hidden.bs.modal", function (event) {
    $('#addEditStudent .error').text("").prop('hidden', true);
    $('#addEditForm').attr("data-id", "");
    $("#groupName").val("");
    $("#student-first-name").val("");
    $("#student-last-name").val("");
    $("#gender-select").val("");
    $("#birthday-input").val("");
  });
    
});