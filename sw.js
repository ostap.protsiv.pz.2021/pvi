const cacheName = 'lab2'
const cacheUrls = [
    'students.html',
    '/js/students.js',
    '/css/styles2.css'
]
self.addEventListener('install', function(event) {
    event.waitUntil(
        caches.open(cacheName)
            .then(function(cache) {
                console.log('Opened cache');
                return cache.addAll(cacheUrls);
            })
    );
});
self.addEventListener('fetch',async(event)=>{
    const response = await getResponse(event.request);
    event.respondWith(response)
})

async function getResponse(request) {
    try {
        const cachedResponse = await caches.match(request);
        if(cachedResponse){
            return cachedResponse;
        }
    
        const fetchedDataResponse = fetch(request);
    
        if (!fetchedDataResponse || fetchedDataResponse.status !== 200 ) {
            return fetchedDataResponse
        
        };
    
        const openedCache = await caches.open(cacheName);
        openedCache.put(request,fetchedDataResponse)
        return fetchedDataResponse;
    } catch (error) {
        console.log(error);
    }


}